spam = ['gato', 'cachorro', 'nasus', 'papagaio'], ['item1', 'item2', 'item3']
print(spam)
print(spam[0])      #primeira lista
print(spam[1])   #segunda lista
print(spam[1][1:2]) #Utiliza um intervalo
print(spam[0][:2])     # vai até o segundo item da lista
print(spam[0][2:])     # começa do segundo item da lista
print('O primeiro item da lista "spam" é: ' + str(spam[0][1:3]))
