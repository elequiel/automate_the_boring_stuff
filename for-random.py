import random
"""
for i in range(1, 11):
    i = random.randint(1, 6)
    print('Dado lançado, numero: ' + str(i))
"""

while True:
    print('Jogarei um dado, escolha um numero entre 1 e 6:')
    num = int(input())
    if num >= 1 and num < 7:
        numRand = random.randint(1, 6)
        if num == numRand :
            print('Parabéns o numero sorteador foi: '+str(num))
        else:
            print('Não foi dessa vez...   :( ')
    else:
        print('Digite um número entre 1 e 6')
